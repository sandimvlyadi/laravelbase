<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@home');
Route::get('page', 'PageController@index')->name('page');
Route::post('ads', 'PageController@ads')->name('ads');
Route::post('message', 'PageController@message')->name('message');
Route::post('subscribe', 'PageController@subscribe')->name('subscribe');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function() {
    Route::post('roles/grid', 'RoleController@grid');
    Route::resource('roles','RoleController');
    Route::post('users/grid', 'UserController@grid');
    Route::resource('users','UserController');
    Route::post('products/grid', 'ProductController@grid');
    Route::resource('products','ProductController');
});