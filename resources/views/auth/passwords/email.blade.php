@extends('layouts.auth')

@section('content')
<div class="login-box">
  <div class="login-logo">
    <a href="{{ url('login') }}"><b>Laravel</b>Base</a>
  </div>
  @if (session('status'))
  <div class="callout callout-success">
    <h4>Success!</h4>
    <p>{{ session('status') }}</p>
  </div>
  @endif
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Reset your password</p>

    <form action="{{ route('password.email') }}" method="post">
      @csrf
      <div class="form-group has-feedback @error('email') has-error @enderror">
        <input id="email" type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email" required autocomplete="email" autofocus>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @error('email')
        <span class="help-block">{{ $message }}</span>
        @enderror
      </div>
      <div class="row">
        <div class="col-xs-4">
          
        </div>
        <!-- /.col -->
        <div class="col-xs-8">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Send Password Reset Link</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
@endsection