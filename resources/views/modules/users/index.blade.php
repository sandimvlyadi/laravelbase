@extends('layouts.grid')

@section('filter')
<div class="form-group">
  <input type="text" name="filter[name]" class="form-control" placeholder="Name" title="Name" data-toggle='tooltip'>
</div>
<div class="form-group">
  <input type="text" name="filter[email]" class="form-control" placeholder="Email" title="Email" data-toggle='tooltip'>
</div>
<div class="form-group">
  <select name="filter[role]" class="form-control select2" data-placeholder="- Pilih Role -">
    {!! \App\Models\Role::options('name') !!}
  </select>
</div>
<button type="reset" class="btn btn-default btn-flat btn-reset" title="Reset" data-toggle='tooltip'><i class="fa fa-refresh"></i></button>
<button type="button" class="btn btn-primary btn-flat btn-search" title="Search" data-toggle='tooltip'><i class="fa fa-search"></i></button>
@can('user-create')
<button type="button" class="btn btn-primary btn-flat pull-right btn-create-page" data-url="{{ route('users.create') }}" title="Create" data-toggle='tooltip'><i class="fa fa-plus"></i> Create User</button>
@endcan
@endsection

@section('js-filters')
    d.name = $("input[name='filter[name]']").val();
    d.email = $("input[name='filter[email]']").val();
    d.role = $("select[name='filter[role]']").val();
@endsection