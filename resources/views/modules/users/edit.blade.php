@extends('layouts.page')

@section('form')
@if($errors->any())
<div class="callout callout-danger error-message">
  <h4>Whoops!</h4>
  There were some problems with your input : 
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<form action="{{ route('users.update',$record->id) }}" method="POST" enctype="multipart/form-data">
  @csrf
  @method('PUT')
  <input type="hidden" name="id" value="{{ $record->id }}">
  <div class="row pp">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <img src="@if(!is_null($record->photopath)) {{ url('storage/'.$record->photopath) }} @else {{ asset('adminlte/dist/img/user.jpg') }} @endif" alt="User Image" class="img-circle">
      <div class="form-group">
        <input type="file" name="photo" style="display:none;" accept="image/*">
        <div class="btn btn-primary btn-change"><i class="fa fa-file-photo-o"></i> Change</div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6">
      <div class="form-group @error('name') has-error @enderror">
        <label>Name:</label>
        <input type="text" name="name" class="form-control required" placeholder="Name" value="{{ $record->name }}">
        @error('name')
        <span class="help-block">{{ $message }}</span>
        @enderror
      </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
      <div class="form-group @error('email') has-error @enderror">
        <label>Email:</label>
        <input type="email" name="email" class="form-control required" placeholder="Email" value="{{ $record->email }}">
        @error('email')
        <span class="help-block">{{ $message }}</span>
        @enderror
      </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
      <div class="form-group @error('password') has-error @enderror">
        <label>Password:</label>
        <input type="password" name="password" class="form-control required" placeholder="Password">
        @error('password')
        <span class="help-block">{{ $message }}</span>
        @enderror
      </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
      <div class="form-group @error('confirm-password') has-error @enderror">
        <label>Confirm Password:</label>
        <input type="password" name="confirm-password" class="form-control required" placeholder="Confirm Password">
        @error('confirm-password')
        <span class="help-block">{{ $message }}</span>
        @enderror
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group @error('roles') has-error @enderror">
        <label>Roles:</label>
        <select name="roles[]" class="form-control select2" data-placeholder="- Pilih Role -" multiple>
          {!! \App\Models\Role::options('name', 'id', ['selected' => $record->roles->pluck('id')->toArray()]) !!}
        </select>
        @error('roles')
        <span class="help-block">{{ $message }}</span>
        @enderror
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
      <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Submit</button>
    </div>
  </div>
</form>
@endsection

@section('script')
<script>
  $(document).on('click', '.btn-change', function(){
    $('input[name="photo"]').trigger('click');
  });

  $(document).on('change', 'input[name="photo"]', function () {
		var thumb = $(this).closest('.col-xs-12.col-sm-12.col-md-12').find('img');

		if(this.files && this.files[0])
		{
			var reader = new FileReader();

			reader.onload = function (e) {
				thumb.attr('src', e.target.result);
			}

			reader.readAsDataURL(this.files[0]);
		}
	});
</script>
@endsection