@extends('layouts.page')

@section('form')
@if($errors->any())
<div class="callout callout-danger error-message">
  <h4>Whoops!</h4>
  There were some problems with your input : 
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
  <div class="row pp">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <img src="@if(!is_null($record->photopath)) {{ url('storage/'.$record->photopath) }} @else {{ asset('adminlte/dist/img/user.jpg') }} @endif" alt="User Image" class="img-circle">
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6">
      <div class="form-group @error('name') has-error @enderror">
        <label>Name:</label>
        <input type="text" name="name" class="form-control required" placeholder="Name" value="{{ $record->name }}" readonly>
        @error('name')
        <span class="help-block">{{ $message }}</span>
        @enderror
      </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
      <div class="form-group @error('email') has-error @enderror">
        <label>Email:</label>
        <input type="email" name="email" class="form-control required" placeholder="Email" value="{{ $record->email }}" readonly>
        @error('email')
        <span class="help-block">{{ $message }}</span>
        @enderror
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group @error('roles') has-error @enderror">
        <label>Roles:</label> <br>
        @foreach($record->roles as $role)
        <div class="label bg-light-blue">{{ $role->name }}</div>
        @endforeach
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
    </div>
  </div>
@endsection