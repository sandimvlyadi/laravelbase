@extends('layouts.grid')

@section('filter')
<div class="form-group">
  <input type="text" name="filter[name]" class="form-control" placeholder="Name" title="Name" data-toggle='tooltip'>
</div>
<div class="form-group">
  <input type="text" name="filter[detail]" class="form-control" placeholder="Detail" title="Detail" data-toggle='tooltip'>
</div>
<button type="reset" class="btn btn-default btn-flat btn-reset" title="Reset" data-toggle='tooltip'><i class="fa fa-refresh"></i></button>
<button type="button" class="btn btn-primary btn-flat btn-search" title="Search" data-toggle='tooltip'><i class="fa fa-search"></i></button>
@can('product-create')
<button type="button" class="btn btn-primary btn-flat pull-right btn-create-page" data-url="{{ route('products.create') }}" title="Create" data-toggle='tooltip'><i class="fa fa-plus"></i> Create Product</button>
@endcan
@endsection

@section('js-filters')
    d.name = $("input[name='filter[name]']").val();
    d.detail = $("input[name='filter[detail]']").val();
@endsection