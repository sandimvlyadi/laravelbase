@extends('layouts.page')

@section('form')
@if($errors->any())
<div class="callout callout-danger error-message">
  <h4>Whoops!</h4>
  There were some problems with your input : 
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group @error('name') has-error @enderror">
        <strong>Name:</strong>
        <input type="text" name="name" class="form-control" placeholder="Name" value="{{ $product->name }}" readonly>
        @error('name')
        <span class="help-block">{{ $message }}</span>
        @enderror
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group @error('detail') has-error @enderror">
        <strong>Detail:</strong>
        <textarea class="form-control" style="height:150px" name="detail" placeholder="Detail" readonly>{{ $product->detail }}</textarea>
        @error('detail')
        <span class="help-block">{{ $message }}</span>
        @enderror
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
    </div>
  </div>
@endsection