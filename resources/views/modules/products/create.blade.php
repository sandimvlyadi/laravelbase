@extends('layouts.page')

@section('form')
@if($errors->any())
<div class="callout callout-danger error-message">
  <h4>Whoops!</h4>
  There were some problems with your input : 
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
  @csrf
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group @error('name') has-error @enderror">
        <label>Name:</label>
        <input type="text" name="name" class="form-control required" placeholder="Name" value="{{ old('name') }}">
        @error('name')
        <span class="help-block">{{ $message }}</span>
        @enderror
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group @error('detail') has-error @enderror">
        <label>Detail:</label>
        <textarea class="form-control required" style="height:150px" name="detail" placeholder="Detail">{{ old('detail') }}</textarea>
        @error('detail')
        <span class="help-block">{{ $message }}</span>
        @enderror
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
      <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Submit</button>
    </div>
  </div>
</form>
@endsection