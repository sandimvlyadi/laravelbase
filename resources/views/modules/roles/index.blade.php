@extends('layouts.grid')

@section('filter')
<div class="form-group">
  <input type="text" class="form-control" style="display:none;">
  <input type="text" name="filter[name]" class="form-control" placeholder="Name" title="Name" data-toggle='tooltip'>
</div>
<button type="reset" class="btn btn-default btn-flat btn-reset" title="Reset" data-toggle='tooltip'><i class="fa fa-refresh"></i></button>
<button type="button" class="btn btn-primary btn-flat btn-search" title="Search" data-toggle='tooltip'><i class="fa fa-search"></i></button>
@can('role-create')
<button type="button" class="btn btn-primary btn-flat pull-right btn-create-page" data-url="{{ route('roles.create') }}" title="Create" data-toggle='tooltip'><i class="fa fa-plus"></i> Create Role</button>
@endcan
@endsection

@section('js-filters')
  d.name = $("input[name='filter[name]']").val();
@endsection