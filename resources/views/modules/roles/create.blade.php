@extends('layouts.page')

@section('form')
@if($errors->any())
<div class="callout callout-danger error-message">
  <h4>Whoops!</h4>
  There were some problems with your input : 
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<form action="{{ route('roles.store') }}" method="POST" enctype="multipart/form-data">
  @csrf
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group @error('name') has-error @enderror">
        <label>Name:</label>
        <input type="text" name="name" class="form-control required" placeholder="Name" value="{{ old('name') }}">
        @error('name')
        <span class="help-block">{{ $message }}</span>
        @enderror
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group @error('permission') has-error @enderror">
        <label>Permissions:</label>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr class="bg-light-blue">
                <th>Name</th>
                <th class="text-center" width="10%">List</th>
                <th class="text-center" width="10%">Create</th>
                <th class="text-center" width="10%">Edit</th>
                <th class="text-center" width="10%">Delete</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Product</td>
                <td class="text-center"><input type="checkbox" name="permission[]" value="{{ $permission['product-list'] }}"></td>
                <td class="text-center"><input type="checkbox" name="permission[]" value="{{ $permission['product-create'] }}"></td>
                <td class="text-center"><input type="checkbox" name="permission[]" value="{{ $permission['product-edit'] }}"></td>
                <td class="text-center"><input type="checkbox" name="permission[]" value="{{ $permission['product-delete'] }}"></td>
              </tr>
              <tr>
                <td>User Management</td>
                <td class="text-center"><input type="checkbox" name="permission[]" value="{{ $permission['user-list'] }}"></td>
                <td class="text-center"><input type="checkbox" name="permission[]" value="{{ $permission['user-create'] }}"></td>
                <td class="text-center"><input type="checkbox" name="permission[]" value="{{ $permission['user-edit'] }}"></td>
                <td class="text-center"><input type="checkbox" name="permission[]" value="{{ $permission['user-delete'] }}"></td>
              </tr>
              <tr>
                <td>Role & Permission</td>
                <td class="text-center"><input type="checkbox" name="permission[]" value="{{ $permission['role-list'] }}"></td>
                <td class="text-center"><input type="checkbox" name="permission[]" value="{{ $permission['role-create'] }}"></td>
                <td class="text-center"><input type="checkbox" name="permission[]" value="{{ $permission['role-edit'] }}"></td>
                <td class="text-center"><input type="checkbox" name="permission[]" value="{{ $permission['role-delete'] }}"></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
      <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Submit</button>
    </div>
  </div>
</form>
@endsection

@section('script')
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
@endsection