@extends('layouts.page')

@section('form')
@if($errors->any())
<div class="callout callout-danger error-message">
  <h4>Whoops!</h4>
  There were some problems with your input : 
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group @error('name') has-error @enderror">
        <label>Name:</label>
        <input type="text" name="name" class="form-control required" placeholder="Name" value="{{ $record->name }}" readonly>
        @error('name')
        <span class="help-block">{{ $message }}</span>
        @enderror
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group @error('permission') has-error @enderror">
        <label>Permissions:</label>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr class="bg-light-blue">
                <th>Name</th>
                <th class="text-center" width="10%">List</th>
                <th class="text-center" width="10%">Create</th>
                <th class="text-center" width="10%">Edit</th>
                <th class="text-center" width="10%">Delete</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Product</td>
                <td class="text-center">@if(in_array($permission['product-list'], $rolePermissions)) <i class="fa fa-check text-primary"></i> @endif</td>
                <td class="text-center">@if(in_array($permission['product-create'], $rolePermissions)) <i class="fa fa-check text-primary"></i> @endif</td>
                <td class="text-center">@if(in_array($permission['product-edit'], $rolePermissions)) <i class="fa fa-check text-primary"></i> @endif</td>
                <td class="text-center">@if(in_array($permission['product-delete'], $rolePermissions)) <i class="fa fa-check text-primary"></i> @endif</td>
              </tr>
              <tr>
                <td>User Management</td>
                <td class="text-center">@if(in_array($permission['user-list'], $rolePermissions)) <i class="fa fa-check text-primary"></i> @endif</td>
                <td class="text-center">@if(in_array($permission['user-create'], $rolePermissions)) <i class="fa fa-check text-primary"></i> @endif</td>
                <td class="text-center">@if(in_array($permission['user-edit'], $rolePermissions)) <i class="fa fa-check text-primary"></i> @endif</td>
                <td class="text-center">@if(in_array($permission['user-delete'], $rolePermissions)) <i class="fa fa-check text-primary"></i> @endif</td>
              </tr>
              <tr>
                <td>Role & Permission</td>
                <td class="text-center">@if(in_array($permission['role-list'], $rolePermissions)) <i class="fa fa-check text-primary"></i> @endif</td>
                <td class="text-center">@if(in_array($permission['role-create'], $rolePermissions)) <i class="fa fa-check text-primary"></i> @endif</td>
                <td class="text-center">@if(in_array($permission['role-edit'], $rolePermissions)) <i class="fa fa-check text-primary"></i> @endif</td>
                <td class="text-center">@if(in_array($permission['role-delete'], $rolePermissions)) <i class="fa fa-check text-primary"></i> @endif</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
    </div>
  </div>
@endsection