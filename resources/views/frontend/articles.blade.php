@extends('frontend.base')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8 articles">
      @for($i=0;$i<5;$i++)
      <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4">
          <a href="#"><img src="{{ asset('adminlte/dist/img/banner/ilovebootstrap3.png') }}" alt="Article Image"></a>
        </div>
        <div class="col-xs-12 col-sm-8 col-md-8">
          <div class="article-title"><a href="{{ url('page?name=article&title=article-title') }}">Article Title</a></div>
          <div class="article-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante. <a href="#">[Read More]</a></div>
          <div class="btn-group">
            <div class="btn btn-article"><i class="fa fa-heart-o"></i></div>
            <div class="btn-group">
              <div class="btn btn-article dropdown-toggle" data-toggle="dropdown"><i class="fa fa-share-alt"></i></div>
              <ul class="dropdown-menu">
                <li><a href="#"><i class="fa fa-facebook-official"></i> Share on Facebook</a></li>
                <li><a href="#"><i class="fa fa-twitter"></i> Make a tweet</a></li>
                <li><a href="#"><i class="fa fa-instagram"></i> Share on Instagram</a></li>
                <li><a href="#"><i class="fa fa-whatsapp"></i> Share to WhatsApp</a></li>
                <li><a href="#"><i class="fa fa-copy"></i> Copy link to clipboard</a></li>
              </ul>
            </div> 
            <div class="btn btn-article"><i class="fa fa-comment-o"></i></div>
          </div>
        </div>
      </div>
      <div class="article-divider"></div>
      @endfor

      <div class="row article-more">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="btn btn-default"><i class="fa fa-refresh"></i> Load More</div>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4 rightbar">
      @include('frontend.rightbar')
    </div>
  </div>
</section>
@endsection