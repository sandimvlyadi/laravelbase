@extends('frontend.base')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8 relations">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <h3>Relations</h3>
        </div>
        @for($i=0;$i<7;$i++)
        <div class="col-xs-6 col-sm-4 col-md-4">
          <a href="#">
            <img src="{{ asset('adminlte/dist/img/banner/ilovebootstrap1.png') }}" class="img-rounded" alt="Article Image">
            <h3>Relation Title</h3>
          </a>
        </div>
        @endfor
        <div class="col-xs-12 col-sm-12 col-md-12">
          <h1><a href="#">Becomes relation?</a></h1>
          <small>Click question above if you want to be related.</small>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4 rightbar">
      @include('frontend.rightbar')
    </div>
  </div>
</section>
@endsection