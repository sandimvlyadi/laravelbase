<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@if(isset($pageTitle)) {{ $pageTitle }} @else {{ config('app.name', 'Laravel') }} @endif</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('adminlte/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('adminlte/dist/css/skins/_all-skins.min.css') }}">
  <!-- Custom Style -->
  <link rel="stylesheet" href="{{ asset('adminlte/dist/css/custom-frontend.css') }}">
  @yield('style')

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav fixed">
  <div class="wrapper">
    <header class="main-header">
      <nav class="navbar navbar-static-top">
        <div class="container">
          <div class="navbar-header">
            <a href="{{ url('/') }}" class="navbar-brand"><b>Laravel</b>Base</a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
              <i class="fa fa-bars"></i>
            </button>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
            <ul class="nav navbar-nav">
              <li @if(isset($activeTab) && $activeTab == 'home') class="active" @endif><a href="{{ url('/') }}">Home <span class="sr-only">(current)</span></a></li>
              <li @if(isset($activeTab) && $activeTab == 'articles') class="active" @endif><a href="{{ url('page?name=articles') }}">Articles</a></li>
              <li @if(isset($activeTab) && $activeTab == 'relations') class="active" @endif><a href="{{ url('page?name=relations') }}">Relations</a></li>
              <li @if(isset($activeTab) && $activeTab == 'products') class="active" @endif><a href="{{ url('page?name=products') }}">Products</a></li>
              <li @if(isset($activeTab) && $activeTab == 'contact-me') class="active" @endif><a href="{{ url('page?name=contact-me') }}">Contact Me</a></li>
            </ul>
          </div>
          <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
      </nav>
    </header>
    <!-- Full Width Column -->
    <div class="content-wrapper">
      <div class="container">
        <!-- Main content -->
        @yield('content')
        <!-- /.content -->
      </div>
      <!-- /.container -->
      <section class="footer">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 address">
              <h3>Contact Me</h3>
              <p>Komp. Permata Biru Blok R.166 <br> Cinunuk, Cileunyi, Bandung, Jawa Barat <br> 40624.</p>
              <p>
                <i class="fa fa-phone-square"></i> <a href="tel:+6287720614000">+62 877 2061 4000</a> <br>
                <i class="fa fa-envelope"></i> <a href="mailto:sandimulyadi@lyzaxx-systema.com">sandimulyadi@lyzaxx-systema.com</a> <br>
              </p>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4">
              <h3>Quick Links</h3>
              <p>
                <a href="{{ url('/') }}"><i class="fa fa-caret-right"></i> Home</a> <br>
                <a href="{{ url('page?name=articles') }}"><i class="fa fa-caret-right"></i> Articles</a> <br>
                <a href="{{ url('page?name=relations') }}"><i class="fa fa-caret-right"></i> Relations</a> <br>
                <a href="{{ url('page?name=products') }}"><i class="fa fa-caret-right"></i> Products</a> <br>
                <a href="{{ url('page?name=contact-me') }}"><i class="fa fa-caret-right"></i> Contact Me</a> <br>
              </p>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4">
              <h3>Get in touch</h3>
              <p>
                Get updates, news and information via my social media accounts: <br>
                <i class="fa fa-facebook-square"></i> <a href="" target="_blank">Lyzaxx Systema</a> <br>
                <i class="fa fa-instagram"></i> <a href="" target="_blank">lyxaxxsystema</a> <br>
                <i class="fa fa-twitter"></i> <a href="" target="_blank">@lyzaxxsystema</a> <br>
                <i class="fa fa-youtube-play"></i> <a href="" target="_blank">Lyzaxx Systema</a> <br>
              </p>
            </div>
          </div>
        </div>
        <!-- /.container -->
      </section>
    </div>
    <!-- /.content-wrapper -->
  </div>
  <!-- ./wrapper -->

  <!-- jQuery 3 -->
  <script src="{{ asset('adminlte/bower_components/jquery/dist/jquery.min.js') }}"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="{{ asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <!-- SlimScroll -->
  <script src="{{ asset('adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
  <!-- FastClick -->
  <script src="{{ asset('adminlte/bower_components/fastclick/lib/fastclick.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('adminlte/dist/js/adminlte.min.js') }}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{ asset('adminlte/dist/js/demo.js') }}"></script>
  <script src="{{ asset('adminlte/plugins/sweetalert/sweetalert.min.js') }}"></script>
  <script>
    $(document).on('keyup', 'input, textarea', function(){
      if ($(this).val().length > 0) {
        $(this).closest('.form-group').removeClass('has-error');
      } else {
        $(this).closest('.form-group').removeClass('has-error');
        $(this).closest('.form-group').addClass('has-error');
      }
    });

    $(document).on('click', 'button[type="submit"]', function(e){
      e.preventDefault();
      var isProcessed = true;
      $(this).closest('form').find('input, textarea').each(function(){
        if ($(this).val() == '' || $(this).val() == 0) {
          isProcessed = false;
        }
      });

      if (isProcessed) {
        swal({
          title: "Kirim Pesan",
          text: "Apakah pesan yang ingin anda kirim sudah sesuai?",
          icon: "info",
          closeOnClickOutside: false,
          dangerMode: false,
          buttons: {
            cancel: {
              text: "Batal",
              value: null,
              visible: true
            },
            confirm: {
              text: "Ya",
              value: true,
              className: "bg-light-blue"
            },
          },
        })
        .then((value) => {
          if (value) {
            $(this).closest('form').trigger('submit');
          }
        });
      } else {
        swal({
          title: "Peringatan!",
          text: "Oops, ada kolom yang masih kosong. Silakan isi semua kolom yang tersedia.",
          icon: "warning",
          button: {
            text: "OK",
            className: "bg-light-blue"
          }
        })
        .then(() => {
          var isFocused = true;
          $(this).closest('form').find('input, textarea').each(function(){
            if ($(this).val() == '' || $(this).val() == 0) {
              if (isFocused) {
                $(this).focus();
                isFocused = false;
              }
              $(this).closest('.form-group').addClass('has-error');
            }
          });
        });
      }
    });

    @error('email')
    $(document).ready(function(){
      swal({
        title: "Peringatan!",
        text: "{{ $message }}",
        icon: "error",
        button: {
          text: "OK",
          className: "bg-light-blue"
        }
      });
    });
    @enderror

    @error('email_subscribe')
    $(document).ready(function(){
      swal({
        title: "Peringatan!",
        text: "{{ $message }}",
        icon: "error",
        button: {
          text: "OK",
          className: "bg-light-blue"
        }
      });
    });
    @enderror

    @error('business_email')
    $(document).ready(function(){
      swal({
        title: "Peringatan!",
        text: "{{ $message }}",
        icon: "error",
        button: {
          text: "OK",
          className: "bg-light-blue"
        }
      });
    });
    @enderror
  </script>
  @yield('script')
</body>
</html>