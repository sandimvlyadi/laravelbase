<div class="row ads-square">
  <a href="#"><img src="{{ asset('adminlte/dist/img/banner/ilovebootstrap1.png') }}" alt="Ads Square Title"></a>
</div>

<div class="row">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Featured Articles</h3>
    </div>
    <div class="box-body">
      <p>
        <a href=""><i class="fa fa-caret-right"></i> Title of featured article</a> <br>
        <a href=""><i class="fa fa-caret-right"></i> Title of featured article</a> <br>
        <a href=""><i class="fa fa-caret-right"></i> Title of featured article</a> <br>
        <a href=""><i class="fa fa-caret-right"></i> Title of featured article</a> <br>
        <a href=""><i class="fa fa-caret-right"></i> Title of featured article</a> <br>
        <a href=""><i class="fa fa-caret-right"></i> Title of featured article</a> <br>
        <a href=""><i class="fa fa-caret-right"></i> Title of featured article</a> <br>
      </p>
    </div>
  </div>
</div>

<div class="row ads-mini">
  <a href="#"><img src="{{ asset('adminlte/dist/img/banner/ilovebootstrap2.png') }}" alt="Ads Square Title"></a>
</div>

<div class="row">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Popular Articles</h3>
    </div>
    <div class="box-body">
      <p>
        <a href=""><i class="fa fa-caret-right"></i> Title of featured article</a> <br>
        <a href=""><i class="fa fa-caret-right"></i> Title of featured article</a> <br>
        <a href=""><i class="fa fa-caret-right"></i> Title of featured article</a> <br>
        <a href=""><i class="fa fa-caret-right"></i> Title of featured article</a> <br>
        <a href=""><i class="fa fa-caret-right"></i> Title of featured article</a> <br>
        <a href=""><i class="fa fa-caret-right"></i> Title of featured article</a> <br>
        <a href=""><i class="fa fa-caret-right"></i> Title of featured article</a> <br>
      </p>
    </div>
  </div>
</div>

<div class="row">
  <div class="box collapsed-box box-primary box-solid">
    <div class="box-header with-border">
      <h3 class="box-title">Call For Ads</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
        </button>
      </div>
    </div>
      <div class="box-body">
        <form action="{{ route('ads') }}" method="post">
          @csrf 
          <input type="text" name="type" value="ads" style="display:none;">
          <div class="form-group @error('business_name') has-error @enderror">
            <label>Your Name</label>
            <input type="text" name="business_name" class="form-control" placeholder="Name" value="{{ old('business_name') }}">
            @error('business_name')
            <span class="help-block">{{ $message }}</span>
            @enderror
          </div>
          <div class="form-group @error('company') has-error @enderror">
            <label>Company Name</label>
            <input type="text" name="company" class="form-control" placeholder="Company" value="{{ old('company') }}">
            @error('company')
            <span class="help-block">{{ $message }}</span>
            @enderror
          </div>
          <div class="form-group @error('business_email') has-error @enderror">
            <label>Email Address</label>
            <input type="email" name="business_email" class="form-control" placeholder="Email" value="{{ old('business_email') }}">
            @error('business_email')
            <span class="help-block">{{ $message }}</span>
            @enderror
          </div>
          <div class="form-group @error('contact') has-error @enderror">
            <label>Business Contact</label>
            <input type="text" name="contact" class="form-control" placeholder="Contact" value="{{ old('contact') }}">
            @error('contact')
            <span class="help-block">{{ $message }}</span>
            @enderror
          </div>
          <div class="form-group @error('description') has-error @enderror">
            <label>Description</label>
            <textarea name="description" rows="4" class="form-control" placeholder="Tell me about your ads">{{ old('description') }}</textarea>
            @error('description')
            <span class="help-block">{{ $message }}</span>
            @enderror
          </div>
          <button type="reset" class="btn btn-default">Reset</button>
          <button type="submit" class="btn btn-primary pull-right">Submit</button>
        </form>
      </div>
  </div>
</div>