@extends('frontend.base')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8 contact-me">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <h3>Contact Me</h3>
          <form action="{{ route('message') }}" method="post">
            @csrf 
            <input type="text" name="type" value="message" style="display:none;">
            <div class="form-group @error('name') has-error @enderror">
              <label>Your Name</label>
              <input type="text" name="name" class="form-control" placeholder="Name" value="{{ old('name') }}">
              @error('name')
              <span class="help-block">{{ $message }}</span>
              @enderror
            </div>
            <div class="form-group @error('email') has-error @enderror">
              <label>Your Email</label>
              <input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}">
              @error('email')
              <span class="help-block">{{ $message }}</span>
              @enderror
            </div>
            <div class="form-group @error('message') has-error @enderror">
              <label>Message</label>
              <textarea name="message" rows="5" class="form-control" placeholder="Type your message here">{{ old('message') }}</textarea>
              @error('message')
              <span class="help-block">{{ $message }}</span>
              @enderror
            </div>
            <button type="reset" class="btn btn-default">Reset</button>
            <button type="submit" class="btn btn-primary pull-right">Submit</button>
          </form>
          <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d7921.1838573729!2d107.7321457!3d-6.939275599999996!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sid!2sid!4v1590163418383!5m2!1sid!2sid" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4 rightbar">
      @include('frontend.rightbar')
    </div>
  </div>
</section>
@endsection