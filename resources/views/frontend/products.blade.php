@extends('frontend.base')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8 products">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <h3>Products</h3>
        </div>
        @for($i=0;$i<7;$i++)
        <div class="col-xs-6 col-sm-4 col-md-4">
          <a href="#">
            <img src="{{ asset('adminlte/dist/img/banner/ilovebootstrap1.png') }}" class="img-rounded" alt="Article Image">
            <h3>Product Title</h3>
            <div class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante. </div>
          </a>
        </div>
        @endfor
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4 rightbar">
      @include('frontend.rightbar')
    </div>
  </div>
</section>
@endsection