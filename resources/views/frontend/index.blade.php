@extends('frontend.base')

@section('content')
<section class="content">
  <div class="row featured">
    <div class="col-xs-12 col-sm-12 col-md-8">
      <div id="slide" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#slide" data-slide-to="0" class="active"></li>
          <li data-target="#slide" data-slide-to="1" class=""></li>
          <li data-target="#slide" data-slide-to="2" class=""></li>
        </ol>
        <div class="carousel-inner">
          <div class="item active">
            <a href=""><img src="{{ asset('adminlte/dist/img/banner/ilovebootstrap1.png') }}" alt="First slide"></a>

            <div class="carousel-caption">
              First Slide
            </div>
          </div>
          <div class="item">
            <a href=""><img src="{{ asset('adminlte/dist/img/banner/ilovebootstrap2.png') }}" alt="Second slide"></a>

            <div class="carousel-caption">
              Second Slide
            </div>
          </div>
          <div class="item">
            <a href=""><img src="{{ asset('adminlte/dist/img/banner/ilovebootstrap3.png') }}" alt="Third slide"></a>

            <div class="carousel-caption">
              Third Slide
            </div>
          </div>
        </div>
        <a class="left carousel-control" href="#slide" data-slide="prev">
          <span class="fa fa-angle-left"></span>
        </a>
        <a class="right carousel-control" href="#slide" data-slide="next">
          <span class="fa fa-angle-right"></span>
        </a>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab_1" data-toggle="tab">Recent</a></li>
          <li><a href="#tab_2" data-toggle="tab">Popular</a></li>
          <li><a href="#tab_3" data-toggle="tab">Products</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tab_1">
            <p><a href="#"><i class="fa fa-caret-right"></i> Put a great title here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
          </div>
          <!-- /.tab-pane -->
          <div class="tab-pane" id="tab_2">
            <p><a href="#"><i class="fa fa-caret-right"></i> Put a great title here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
          </div>
          <!-- /.tab-pane -->
          <div class="tab-pane" id="tab_3">
            <p><a href="#"><i class="fa fa-caret-right"></i> Put a great title here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
            <p><a href="#"><i class="fa fa-caret-right"></i> Another great title you should put here.</a></p>
          </div>
          <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
      </div>
    </div>
  </div>
  <div class="row relations">
    <h3>My Relations</h3>
    <div class="col-xs-4 col-sm-2 col-md-2">
      <img src="{{ asset('adminlte/dist/img/user2-160x160.jpg') }}">
    </div>
    <div class="col-xs-4 col-sm-2 col-md-2">
      <img src="{{ asset('adminlte/dist/img/user2-160x160.jpg') }}">
    </div>
    <div class="col-xs-4 col-sm-2 col-md-2">
      <img src="{{ asset('adminlte/dist/img/user2-160x160.jpg') }}">
    </div>
    <div class="col-xs-4 col-sm-2 col-md-2">
      <img src="{{ asset('adminlte/dist/img/user2-160x160.jpg') }}">
    </div>
    <div class="col-xs-4 col-sm-2 col-md-2">
      <img src="{{ asset('adminlte/dist/img/user2-160x160.jpg') }}">
    </div>
    <div class="col-xs-4 col-sm-2 col-md-2 vcenter">
      <a href="{{ url('page?name=relations') }}" class="more">
        <i class="fa fa-chevron-right"></i>
        <p>See More</p>
      </a>
    </div>
  </div>
  <div class="row testimony">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div id="testimony" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="item active">
            <blockquote>
              <p>Kind programmer, fast response, and could work fastly.</p>
              <p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></p>
              <small><cite title="Sandi Mulyadi">Sandi Mulyadi as CEO Lyzaxx Systema</cite></small>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <p>Kind programmer, fast response, and could work fastly.</p>
              <p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></p>
              <small><cite title="Sandi Mulyadi">Sandi Mulyadi</cite></small>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <p>Kind programmer, fast response, and could work fastly.</p>
              <p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></p>
              <small><cite title="Sandi Mulyadi">Sandi Mulyadi</cite></small>
            </blockquote>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row text-one">
    <div class="col-xs-12 col-sm-12 col-md-6 vcenter">
      <p>Laravel is one of most best php web framework which I recently used to build some awesome web project. With Laravel, I could build web project fastly. I also build a base for me to start up a project workflow. If you want to try it, you should follow this <a href="" target="_blank">link <i class="fa fa-external-link"></i></a> on github.</p>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
      <img src="{{ asset('adminlte/dist/img/banner/ilovebootstrap1.png') }}" alt="First slide">
    </div>
  </div>
  <div class="row text-two">
    <div class="col-xs-12 col-sm-12 col-md-6">
      <img src="{{ asset('adminlte/dist/img/banner/ilovebootstrap1.png') }}" alt="First slide">
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 vcenter">
      <p>I've some project to build application in mobile. To build a warehouse application, I usually write in C# for windows compact framework. Sometime, I get some order to build android apps. So, I also could code in java android.</p>
    </div>
  </div>
  <div class="row text-two-mobile">
    <div class="col-xs-12 col-sm-12 col-md-6 vcenter">
      <p>I've some project to build application in mobile. To build a warehouse application, I usually write in C# for windows compact framework. Sometime, I get some order to build android apps. So, I also could code in java android.</p>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
      <img src="{{ asset('adminlte/dist/img/banner/ilovebootstrap1.png') }}" alt="First slide">
    </div>
  </div>
  <div class="row text-three">
    <div class="col-xs-12 col-sm-12 col-md-6 vcenter">
      <p>As an fullstack programmer, I must have knowladge to understanding server. So, I also could maintenance server based on linux system. Mostly, client hires me to publish project to their server too. So, I should do that.</p>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
      <img src="{{ asset('adminlte/dist/img/banner/ilovebootstrap1.png') }}" alt="First slide">
    </div>
  </div>
  <div class="row work">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <h3>I've been code</h3>
      <div id="product" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="item active">
            <div class="row">
              <div class="col-xs-3 col-sm-3 col-md-3">
                <a href="">
                  <img src="{{ asset('adminlte/dist/img/user2-160x160.jpg') }}">
                </a>
              </div>
              <div class="col-xs-3 col-sm-3 col-md-3">
                <a href="">
                  <img src="{{ asset('adminlte/dist/img/user2-160x160.jpg') }}">
                </a>
              </div>
              <div class="col-xs-3 col-sm-3 col-md-3">
                <a href="">
                  <img src="{{ asset('adminlte/dist/img/user2-160x160.jpg') }}">
                </a>
              </div>
              <div class="col-xs-3 col-sm-3 col-md-3">
                <a href="">
                  <img src="{{ asset('adminlte/dist/img/user2-160x160.jpg') }}">
                </a>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="row">
              <div class="col-xs-3 col-sm-3 col-md-3">
                <a href="">
                  <img src="{{ asset('adminlte/dist/img/user2-160x160.jpg') }}">
                </a>
              </div>
              <div class="col-xs-3 col-sm-3 col-md-3">
                <a href="">
                  <img src="{{ asset('adminlte/dist/img/user2-160x160.jpg') }}">
                </a>
              </div>
              <div class="col-xs-3 col-sm-3 col-md-3">
                <a href="">
                  <img src="{{ asset('adminlte/dist/img/user2-160x160.jpg') }}">
                </a>
              </div>
              <div class="col-xs-3 col-sm-3 col-md-3">
                <a href="">
                  <img src="{{ asset('adminlte/dist/img/user2-160x160.jpg') }}">
                </a>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="row">
              <div class="col-xs-3 col-sm-3 col-md-3">
                <a href="">
                  <img src="{{ asset('adminlte/dist/img/user2-160x160.jpg') }}">
                </a>
              </div>
              <div class="col-xs-3 col-sm-3 col-md-3">
                <a href="">
                  <img src="{{ asset('adminlte/dist/img/user2-160x160.jpg') }}">
                </a>
              </div>
              <div class="col-xs-3 col-sm-3 col-md-3">
                <a href="">
                  <img src="{{ asset('adminlte/dist/img/user2-160x160.jpg') }}">
                </a>
              </div>
              <div class="col-xs-3 col-sm-3 col-md-3">
                <a href="">
                  <img src="{{ asset('adminlte/dist/img/user2-160x160.jpg') }}">
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row email-subscription">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <h3>Email Subscription</h3>
      <div class="form-group @error('email_subscribe') has-error @enderror">
        <form action="{{ route('subscribe') }}" method="post">
          @csrf 
          <input type="text" name="type" value="subscribe" style="display:none;">
          <div class="input-group input-group-lg">
            <input type="text" name="email_subscribe" class="form-control" placeholder="Your email here. . . " value="{{ old('email_subscribe') }}">
            <div class="input-group-btn">
              <button type="submit" class="btn btn-primary @error('email_subscribe') btn-danger @enderror">Subscribe</button>
            </div>
          </div>
        </form>
        @error('email_subscribe')
        <span class="help-block">{{ $message }}</span>
        @enderror
      </div>
    </div>
  </div>
  <div class="row find-me">
    <div class="col-xs-12 col-sm-6 col-md-6 contact">
      <h3>Find to talk with me</h3>
      <form action="{{ route('message') }}" method="post">
        @csrf 
        <input type="text" name="type" value="message" style="display:none;">
        <div class="form-group @error('name') has-error @enderror">
          <label>Your Name</label>
          <input type="text" name="name" class="form-control" placeholder="Your fullname" value="{{ old('name') }}">
          @error('name')
          <span class="help-block">{{ $message }}</span>
          @enderror
        </div>
        <div class="form-group @error('email') has-error @enderror">
          <label>Your Email</label>
          <input type="email" name="email" class="form-control" placeholder="Your email address" value="{{ old('email') }}">
          @error('email')
          <span class="help-block">{{ $message }}</span>
          @enderror
        </div>
        <div class="form-group @error('message') has-error @enderror">
          <label>Message</label>
          <textarea name="message" rows="5" placeholder="Talk to me about anything" class="form-control">{{ old('message') }}</textarea>
          @error('message')
          <span class="help-block">{{ $message }}</span>
          @enderror
        </div>
        <button type="reset" class="btn btn-default">Reset</button>
        <button type="submit" class="btn btn-primary pull-right">Submit</button>
      </form>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d7921.1838573729!2d107.7321457!3d-6.939275599999996!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sid!2sid!4v1590163418383!5m2!1sid!2sid" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>
  </div>
  <div class="row quotes">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div id="quotes" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="item active">
            <blockquote>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
              <small><cite title="Sandi Mulyadi">Sandi Mulyadi</cite></small>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
              <small><cite title="Sandi Mulyadi">Sandi Mulyadi</cite></small>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
              <small><cite title="Sandi Mulyadi">Sandi Mulyadi</cite></small>
            </blockquote>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection