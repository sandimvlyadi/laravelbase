@extends('frontend.base')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8 article-single">
      <div class="row">
        <img src="{{ asset('adminlte/dist/img/banner/ilovebootstrap3.png') }}" alt="Article Image">
        <div class="article-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
        <small>
          <i class="fa fa-clock-o"></i> Published on May 27, 2020 at 06:54 <br>
          <i class="fa fa-user"></i> Writted by <a href="#">Sandi Mulyadi</a> <br>
          <i class="fa fa-tags"></i> Category : <a href="#">News</a>, <a href="#">Linux</a>
        </small>
      </div>
      <div class="row article-post">
        <h3>What is Lorem Ipsum?</h3>
        <b>Lorem Ipsum</b> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
        <h3>Where does it come from?</h3>
        Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. <br>
        The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
        <h3>Why do we use it?</h3>
        It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
        <h3>Where can I get some?</h3>
        There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
      </div>
      <div class="row tags">
        <h3>Tags :</h3>
        <a href="#" class="tag">#Ubuntu</a> <a href="#" class="tag">#20.04</a> <a href="#" class="tag">#Update</a>
      </div>
      <div class="row share">
        <h3>Share :</h3>
        <div class="row">
          <div class="col-xs-4 col-sm-2 col-md-2">
            <div class="btn btn-article"><i class="fa fa-facebook-official"></i> Facebook</div>
          </div>
          <div class="col-xs-4 col-sm-2 col-md-2">
            <div class="btn btn-article"><i class="fa fa-twitter"></i> Twitter</div>
          </div>
          <div class="col-xs-4 col-sm-2 col-md-2">
            <div class="btn btn-article"><i class="fa fa-instagram"></i> Instagram</div>
          </div>
          <div class="col-xs-4 col-sm-2 col-md-2">
            <div class="btn btn-article"><i class="fa fa-whatsapp"></i> WhatsApp</div>
          </div>
          <div class="col-xs-4 col-sm-2 col-md-2">
            <div class="btn btn-article"><i class="fa fa-copy"></i> Copy Link</div>
          </div>
          <div class="col-xs-4 col-sm-2 col-md-2">
            <div class="btn btn-article"><i class="fa fa-heart loved"></i> Loved</div>
          </div>
        </div>
      </div>
      <div class="row discussion">
        <h3>Discussion :</h3>
        <div class="row">
          <div class="col-xs-6 col-sm-3 col-md-3">
            <a class="btn btn-xs btn-block btn-social btn-facebook">
              <i class="fa fa-facebook"></i> Sign in with Facebook
            </a>
          </div>
          <div class="col-xs-6 col-sm-3 col-md-3">
            <a class="btn btn-xs btn-block btn-social btn-twitter">
              <i class="fa fa-twitter"></i> Sign in with Twitter
            </a>
          </div>
          <div class="col-xs-6 col-sm-3 col-md-3">
            <a class="btn btn-xs btn-block btn-social btn-instagram">
              <i class="fa fa-instagram"></i> Sign in with Instagram
            </a>
          </div>
          <div class="col-xs-6 col-sm-3 col-md-3">
            <a class="btn btn-xs btn-block btn-social btn-google">
              <i class="fa fa-google"></i> Sign in with Google
            </a>
          </div>
        </div>
        <div class="row post-comment">
          <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="form-group">
              <input type="text" name="name" class="form-control" placeholder="Name">
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="form-group">
              <input type="text" name="email" class="form-control" placeholder="Email">
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <textarea name="comment" rows="4" class="form-control" placeholder="Write anything to dicsuss about this article"></textarea>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="btn btn-primary pull-right">Submit</div>
          </div>
        </div>
        <div class="row discussions">
          <div class="box box-widget">
            <div class="box-footer box-comments">
              <div class="box-comment">
                <img class="img-circle img-sm" src="{{ asset('adminlte/dist/img/user3-128x128.jpg') }}" alt="User Image">
                <div class="comment-text">
                  <span class="username">
                    Maria Gonzales
                    <p class="text-muted"><i class="fa fa-clock-o"></i> 8:03 PM Today</p>
                  </span>
                  <p>It is a long established fact that a reader will be distracted
                  by the readable content of a page when looking at its layout.</p>
                  <span class="comment-action">
                    <a href="#"><i class="fa fa-reply"></i> Reply</a> <a href="#"><i class="fa fa-heart-o"></i> Give a Love</a>
                  </span>
                  <div class="row discussions">
                    <div class="box box-widget">
                      <div class="box-footer box-comments">
                        <div class="box-comment">
                          <img class="img-circle img-sm" src="{{ asset('adminlte/dist/img/user3-128x128.jpg') }}" alt="User Image">
                          <div class="comment-text">
                            <span class="username">
                              Maria Gonzales
                              <p class="text-muted"><i class="fa fa-clock-o"></i> 8:03 PM Today</p>
                            </span>
                            <p>It is a long established fact that a reader will be distracted
                            by the readable content of a page when looking at its layout.</p>
                            <span class="comment-action">
                              <a href="#"><i class="fa fa-reply"></i> Reply</a> <a href="#"><i class="fa fa-heart-o"></i> Give a Love</a>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row discussions">
          <div class="box box-widget">
            <div class="box-footer box-comments">
              <div class="box-comment">
                <img class="img-circle img-sm" src="{{ asset('adminlte/dist/img/user3-128x128.jpg') }}" alt="User Image">
                <div class="comment-text">
                  <span class="username">
                    Maria Gonzales
                    <p class="text-muted"><i class="fa fa-clock-o"></i> 8:03 PM Today</p>
                  </span>
                  <p>It is a long established fact that a reader will be distracted
                  by the readable content of a page when looking at its layout.</p>
                  <span class="comment-action">
                    <a href="#"><i class="fa fa-reply"></i> Reply</a> <a href="#"><i class="fa fa-heart-o"></i> Give a Love</a>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4 rightbar">
      @include('frontend.rightbar')
    </div>
  </div>
</section>
@endsection