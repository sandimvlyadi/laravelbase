<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'Laravel') }} | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/square/blue.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/jvectormap/jquery-jvectormap.css') }}">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('adminlte/dist/css/AdminLTE.min.css') }}">
  <!-- Custom Style -->
  <link rel="stylesheet" href="{{ asset('adminlte/dist/css/custom.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('adminlte/dist/css/skins/_all-skins.min.css') }}">
  @yield('style')

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue fixed">
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href="{{ route('home') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>L</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Laravel</b>Base</span>
    </a>

    @include('layouts.partials.header-navbar')
  </header>
  @include('layouts.partials.aside')

  <!-- Content Wrapper. Contains page content -->
  @yield('content')
  <!-- /.content-wrapper -->

  @include('layouts.partials.footer')

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{ asset('adminlte/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('adminlte/bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('adminlte/dist/js/adminlte.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('adminlte/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap  -->
<script src="{{ asset('adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('adminlte/bower_components/chart.js/Chart.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('adminlte/dist/js/demo.js') }}"></script>
<script src="{{ asset('adminlte/plugins/sweetalert/sweetalert.min.js') }}"></script>
<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree();
    var pageLink = '{{ $pageLink }}';
    $('ul.sidebar-menu').find('a[href="'+pageLink+'"]').parent().addClass('active');
    $('ul.sidebar-menu').find('a[href="'+pageLink+'"]').closest('.treeview').addClass('active');

    setTimeout(() => {
      $('.error-message').slideUp('normal', function(){
        $(this).remove();
      });
      $('.success-message').slideUp('normal', function(){
        $(this).remove();
      });
    }, 3000);

    $('.select2').select2({
      placeholder: function(){
        $(this).data('placeholder');
      }
    });

    $(function () {
      $('[data-toggle="tooltip"]').tooltip();
    });
  });

  $(document).on('keyup', '.required', function(){
    var val = $(this).val();
    if (val == '' || val == 0) {
      $(this).parent().find('span').slideDown();
      if (!$(this).parent().hasClass('has-error')) {
        $(this).parent().addClass('has-error');
      }
    } else{
      $(this).parent().find('span').slideUp();
      if ($(this).parent().hasClass('has-error')) {
        $(this).parent().removeClass('has-error');
      }
    }
  });

  $(document).on('click', 'button[type="submit"]', function(e){
    e.preventDefault();
    swal({
      title: "Submit Data",
      text: "Apakah anda yakin ingin mengirim data ini?",
      icon: "info",
      closeOnClickOutside: false,
      dangerMode: false,
      buttons: {
        cancel: {
          text: "Batal",
          value: null,
          visible: true
        },
        confirm: {
          text: "Ya",
          value: true,
          className: "bg-light-blue"
        },
      },
    })
    .then((value) => {
      if (value) {
        $(this).closest('form').trigger('submit');
      }
    });
  });
</script>
@yield('script')
</body>
</html>