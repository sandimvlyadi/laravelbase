<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 1.0.1
  </div>
  <strong>Copyright &copy; 2020 @if(date('Y')>2020) {{ "-" . date('Y') }} @endif <a href="https://www.lyzaxx-systema.com/" target="_blank">Lyzaxx Systema</a>.</strong> All rights
  reserved.
</footer>