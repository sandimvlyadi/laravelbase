<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    {{ $title ?? '' }}
    <small>{{ $subtitle ?? '' }}</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('home') }}" title="Go to home" data-toggle='tooltip'><i class="fa fa-home"></i></a></li>
    <?php $i=1;$last=count($breadcrumb);?>
    @foreach( $breadcrumb as $name => $link )
      @if( $i++ != $last )
        <li><a href="{{ $link }}">{{ $name }}</a></li>
      @else
        <li class="active">{{ $name }}</li>
      @endif
    @endforeach        
  </ol>
</section>