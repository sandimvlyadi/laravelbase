@extends('layouts.base')

@section('content')
  <div class="content-wrapper">
    @include('layouts.partials.content-header')

    <!-- Main content -->
    <section class="content">
      @if ($message = Session::get('success'))
      <div class="callout callout-success success-message">
        <h4>Success!</h4>
        <p>{{ $message }}</p>
      </div>
      @endif
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <form class="filter form-inline">
                @yield('filter')
              </form>
            </div>
            <!-- /.box-header -->
            @if(isset($tableStruct))
            <div class="box-body table-responsive">
              <table id="dt" class="table table-bordered table-striped table-hover">
                <thead>
                  <tr class="bg-light-blue">
                    @foreach ($tableStruct as $struct)
                      <th class="text-center">{!! $struct['label'] !!}</th>
                    @endforeach
                  </tr>
                </thead>
                <tbody>
                    @yield('tableBody')
                </tbody>
              </table>
            </div>
            @endif
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script')
<script>
  $(document).ready(function(){
    var dt = '';
    dt = $('#dt').DataTable({
      dom: 'rt<"bottom"ip><"clear">',
			responsive: true,
			autoWidth: false,
			processing: true,
			@if(!$mockup)
			serverSide: true,
			@endif
			lengthChange: false,
			pageLength: 10,
			filter: false,
			sorting: [],
			language: {
				url: "{{ asset('adminlte/plugins/datatables/Indonesian.json') }}"
			},
			@if(!$mockup)
			ajax:  {
				url: "{{ url($pageLink) }}/grid",
				type: 'POST',
				data: function (d) {
					d._token = "{{ csrf_token() }}";
					@yield('js-filters')
				}
			}, 
			@endif
			columns: {!! json_encode($tableStruct) !!},
			drawCallback: function() {
				var api = this.api();
				api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					cell.innerHTML = parseInt(cell.innerHTML)+i+1;
        } );

        $(function () {
          $('[data-toggle="tooltip"]').tooltip();
        });
        
				@if(isset($footer_total))
          var json = api.ajax.json();
          console.log(json);
          $(this).append(@yield('total'));
        @endif
			}
    });
    
    $('.btn-search').on('click', function(e) {
			dt.draw();
			e.preventDefault();
    });
    
    $('.btn-reset').on('click', function(e) {
      $(this).closest('form').find('select').val('').trigger('change');
			setTimeout(function(){
				dt.draw();
			}, 100);
    });
    
    $('.btn-create-page').on('click', function(e) {
      var url = $(this).data('url');
      window.location.href = url;
		});

    $('.filter').on('keyup', function(e){
      if (e.keyCode == 13) {
        $('.btn-search').trigger('click');
      }
    });

    $(document).on('click', '.btn-edit-page, .btn-show-page', function(e) {
      var url = $(this).data('url');
      window.location.href = url;
    });

    $(document).on('click', '.btn-delete', function(){
      swal({
        title: "Hapus Data",
        text: "Apakah anda yakin ingin menghapus data ini?",
        icon: "warning",
        closeOnClickOutside: false,
        dangerMode: true,
        buttons: {
          cancel: {
            text: "Batal",
            value: null,
            visible: true
          },
          confirm: {
            text: "Ya",
            value: true
          },
        },
      })
      .then((value) => {
        if (value) {
          var url = $(this).data('url');
          $.ajax({
            url: url,
            type: 'POST',
            data: {
              '_method' : 'DELETE',
              '_token' : '{{ csrf_token() }}'
            }
          }).done(function(response) {
            swal({
              title: "Berhasil",
              text: "Data berhasil dihapus.",
              icon: "success",
              closeOnClickOutside: false
            })
            .then((value) => {
              dt.draw();
            });
          }).fail(function(response) {
            swal({
              title: "Gagal",
              text: "Data gagal dihapus.",
              icon: "error",
              closeOnClickOutside: false
            })
            .then((value) => {
              dt.draw();
            });
          });
        }
      });
    });
  });
</script>
@endsection