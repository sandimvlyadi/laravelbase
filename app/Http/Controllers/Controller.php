<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private $pageTitle = '';
    private $title = '';
    private $subtitle = '';
    private $breadcrumb = [];
    private $pageLink = '';
    private $tableStruct = [];

    public function setPageTitle($value=[])
    {
        $this->pageTitle = $value;
    }

    public function getPageTitle()
    {
        return $this->pageTitle;
    }

    public function setTitle($value=[])
    {
        $this->title = $value;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setSubtitle($value=[])
    {
        $this->subtitle = $value;
    }

    public function getSubtitle()
    {
        return $this->subtitle;
    }

    public function setBreadcrumb($value=[])
    {
        $this->breadcrumb = $value;
    }

    public function pushBreadCrumb($value=[])
    {
        $this->setBreadcrumb(array_merge($this->breadcrumb, $value));
    }

    public function getBreadcrumb()
    {
        return $this->breadcrumb;
    }

    public function setPageLink($value=[])
    {
        $this->pageLink = $value;
    }

    public function getPageLink()
    {
        return $this->pageLink;
    }

    public function setTableStruct($value=[])
    {
    	$this->tableStruct = $value;
    }

    public function getTableStruct()
    {
    	return $this->tableStruct;
    }

    public function render($view, $additional=[])
    {
        $data = [
            'pageTitle'     => $this->pageTitle,
            'title'         => $this->title,
            'subtitle'      => $this->subtitle,
            'breadcrumb'    => $this->breadcrumb,
            'pageLink'      => $this->pageLink,
            'tableStruct'   => $this->tableStruct,
        ];

        return view($view, array_merge($data, $additional));
    }

    public function makeButton($params = [])
    {
        $settings = [
            'class'    => 'btn btn-primary',
            'label'    => 'Button',
            'tooltip'  => '',
            'target'   => url('/'),
            'disabled' => '',
        ];

        $btn   = '';
        $datas = '';
        $attrs = '';

        if (isset($params['datas'])) {
            foreach ($params['datas'] as $k => $v) {
                $datas .= " data-{$k}=\"{$v}\"";
            }
        }

        if (isset($params['attributes'])) {
            foreach ($params['attributes'] as $k => $v) {
                $attrs .= " {$k}=\"{$v}\"";
            }
        }

        switch ($params['type']) {
            case "delete":
                $settings['class']   = 'btn btn-danger';
                $settings['label']   = '<i class="fa fa-trash"></i>';
                $settings['tooltip'] = 'Delete';

                $params  = array_merge($settings, $params);
                $extends = " title='{$params['tooltip']}' data-id='{$params['id']}'";

                $btn = "<button type='button' {$datas}{$attrs}{$extends} class='{$params['class']} btn-xs btn-delete' data-toggle='tooltip'>{$params['label']}</button>\n";
                break;
            case "edit":
                $settings['class']   = 'btn btn-default';
                $settings['label']   = '<i class="fa fa-pencil"></i>';
                $settings['tooltip'] = 'Change';

                $params  = array_merge($settings, $params);
                $extends = " title='{$params['tooltip']}' data-id='{$params['id']}'";

                $btn = "<button type='button' {$datas}{$attrs}{$extends} class='{$params['class']} btn-xs btn-edit' data-toggle='tooltip'>{$params['label']}</button>\n";
                break;
            case "edit-page":
                $settings['class']   = 'btn btn-default';
                $settings['label']   = '<i class="fa fa-pencil"></i>';
                $settings['tooltip'] = 'Change';

                $params  = array_merge($settings, $params);
                $extends = " title='{$params['tooltip']}' data-id='{$params['id']}'";

                $btn = "<button type='button' {$datas}{$attrs}{$extends} class='{$params['class']} btn-xs btn-edit-page' data-toggle='tooltip'>{$params['label']}</button>\n";
                break;
            case "show-page":
                $settings['class']   = 'btn btn-info';
                $settings['label']   = '<i class="fa fa-eye"></i>';
                $settings['tooltip'] = 'Detail';

                $params  = array_merge($settings, $params);
                $extends = " title='{$params['tooltip']}' data-id='{$params['id']}'";

                $btn = "<button type='button' {$datas}{$attrs}{$extends} class='{$params['class']} btn-xs btn-show-page' data-toggle='tooltip'>{$params['label']}</button>\n";
                break;
            case "details-page":
                $settings['class']   = 'btn btn-primary';
                $settings['label']   = '<i class="fa fa-eye"></i>';
                $settings['tooltip'] = 'Detail';

                $params  = array_merge($settings, $params);
                $extends = " title='{$params['tooltip']}' data-id='{$params['id']}'";

                $btn = "<button type='button' {$datas}{$attrs}{$extends} class='{$params['class']} btn-xs' data-toggle='tooltip'>{$params['label']}</button>\n";
                break;
            case "modal":
                $settings['class']   = 'btn btn-default';
                $settings['label']   = '<i class="fa fa-pencil"></i>';
                $settings['tooltip'] = 'Change';

                $params  = array_merge($settings, $params);
                $extends = " title='{$params['tooltip']}' data-id='{$params['id']}'";

                $btn = "<button type='button' {$datas}{$attrs}{$extends} class='{$params['class']} btn-xs' data-toggle='tooltip'>{$params['label']}</button>\n";
                break;
            case "modal_user":
                $settings['class']   = "btn btn-success";
                $settings['label']   = '<i class="fa fa-user"></i><i class="fa fa-plus"></i>';
                $settings['tooltip'] = 'Create User';

                $params  = array_merge($settings, $params);
                $extends = " title='{$params['tooltip']}' data-id='{$params['id']}'";

                $btn = "<button type='button' {$datas}{$attrs}{$extends} class='{$params['class']} btn-xs' data-toggle='tooltip'>{$params['label']}</button>\n";
                break;
            case "url":
            default:
                $settings['class']   = 'btn btn-primary';
                $settings['label']   = '<i class="fa fa-eye"></i>';

                $params  = array_merge($settings, $params);
                $extends = '';
                if($params['tooltip'] != '')
                {
                    $extends = " title='{$params['tooltip']}'";
                }

                $btn = "<a href='{$params['target']}' {$datas}{$attrs}{$extends} class='{$params['class']} btn-xs' data-toggle='tooltip'>{$params['label']}</a>\n";
        }

        return $btn;
    }

    // ------- FRONTEND SECTION ------- //

    private $activeTab = '';

    public function setActiveTab($value=[])
    {
        $this->activeTab = $value;
    }

    public function getActiveTab()
    {
        return $this->activeTab;
    }

    public function frontend($view, $additional=[])
    {
        $data = [
            'pageTitle'     => $this->pageTitle,
            'title'         => $this->title,
            'pageLink'      => $this->pageLink,
            'activeTab'     => $this->activeTab,
        ];

        return view($view, array_merge($data, $additional));
    }
}
