<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\Permission;
use Illuminate\Http\Request;
use App\Http\Requests\Role\RoleRequest;

use DataTables;

class RoleController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index','store']]);
        $this->middleware('permission:role-create', ['only' => ['create','store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);

        $this->setPageTitle('Role');
        $this->setTitle('Role');
        $this->setSubtitle('');
        $this->setBreadcrumb(['Role' => '#']);
        $this->setPageLink( url('roles') );

        $this->setTableStruct([
            [
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
                'searchable' => false,
                'class' => 'text-center',
                'width' => '5%'
			],
            [
			    'data' => 'name',
			    'name' => 'name',
			    'label' => 'Name',
			    'searchable' => false,
			    'sortable' => true
			],
            [
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Action',
			    'searchable' => false,
                'sortable' => false,
                'class' => 'text-center',
                'width' => '10%'
			]
        ]);
    }

    public function grid(Request $request)
    {
        $records = Role::select('*');

        // Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('id', 'desc');
        }

        // Filters
        if ($name = $request->name) {
            $records->where('name', 'like', '%' . $name . '%');
        }

        return Datatables::of($records)
                ->addColumn('num', function ($record) use ($request) {
                    return $request->get('start');
                })
                ->addColumn('action', function ($record) {
                    $btn = '';

                    //Detail
                    $btn .= $this->makeButton([
                        'type' => 'show-page',
                        'id' => $record->id,
                        'datas' => ['url' => route('roles.show',$record->id)]
                    ]);

                    //Edit
                    $btn .= $this->makeButton([
                        'type' => 'edit-page',
                        'id' => $record->id,
                        'datas' => ['url' => route('roles.edit',$record->id)]
                    ]);

                    if ($record->isDeleteable()) {
                        // Delete
                        $btn .= $this->makeButton([
                            'type' => 'delete',
                            'id'   => $record->id,
                            'datas' => ['url' => route('roles.destroy',$record->id)]
                        ]);
                    }

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
    }

    public function index(Request $request)
    {
        return $this->render('modules.roles.index', [
            'mockup' => false
        ]);
    }

    public function create()
    {
        $permission = Permission::allPermission();
        $this->setTitle('Create Role');
        $this->pushBreadcrumb(['Create Role' => '#']);
        return $this->render('modules.roles.create', [
            'permission' => $permission
        ]);
    }

    public function store(RoleRequest $request)
    {
        $role = new Role;
        $role->simpan($request);

        return redirect()->route('roles.index')
                        ->with('success','Role created successfully.');
    }

    public function show(Role $role)
    {
        $permission = Permission::allPermission();
        $this->setTitle('Detail Role');
        $this->pushBreadcrumb(['Detail Role' => '#']);
        return $this->render('modules.roles.show', [
            'record' => $role,
            'permission' => $permission,
            'rolePermissions' => $role->perms()
        ]);
    }

    public function edit(Role $role)
    {
        $permission = Permission::allPermission();
        $this->setTitle('Change Role');
        $this->pushBreadcrumb(['Change Role' => '#']);
        return $this->render('modules.roles.edit', [
            'record' => $role,
            'permission' => $permission,
            'rolePermissions' => $role->perms()
        ]);
    }

    public function update(RoleRequest $request, Role $role)
    {
        $role->ubah($request);

        return redirect()->route('roles.index')
                        ->with('success','Role updated successfully');
    }

    public function destroy(Role $role)
    {
        $role->hapus();

        return redirect()->route('roles.index')
                        ->with('success','Role deleted successfully');
    }
}