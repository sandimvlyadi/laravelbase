<?php

namespace App\Http\Controllers;

use App\Models\Product\Product;
use Illuminate\Http\Request;
use App\Http\Requests\Product\ProductRequest;

use DataTables;

class ProductController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:product-list|product-create|product-edit|product-delete', ['only' => ['index','show']]);
        $this->middleware('permission:product-create', ['only' => ['create','store']]);
        $this->middleware('permission:product-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:product-delete', ['only' => ['destroy']]);

        $this->setPageTitle('Product');
        $this->setTitle('Product');
        $this->setSubtitle('');
        $this->setBreadcrumb(['Product' => '#']);
        $this->setPageLink( url('products') );

        $this->setTableStruct([
            [
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
                'searchable' => false,
                'class' => 'text-center',
                'width' => '5%'
			],
            [
			    'data' => 'name',
			    'name' => 'name',
			    'label' => 'Name',
			    'searchable' => false,
			    'sortable' => true
			],
            [
                'data' => 'detail',
                'name' => 'detail',
                'label' => 'Detail',
                'searchable' => false,
                'sortable' => true
            ],
            [
                'data' => 'created',
                'name' => 'created',
                'label' => 'Created',
                'searchable' => false,
                'sortable' => false,
                'width' => '15%'
            ],
            [
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Action',
			    'searchable' => false,
                'sortable' => false,
                'class' => 'text-center',
                'width' => '10%'
			]
        ]);
    }
    
    public function grid(Request $request)
    {
        $records = Product::select('*');

        // Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('id', 'desc');
        }

        // Filters
        if ($name = $request->name) {
            $records->where('name', 'like', '%' . $name . '%');
        }
        if ($detail = $request->detail) {
            $records->where('detail', 'like', '%' . $detail . '%');
        }

        return Datatables::of($records)
                ->addColumn('num', function ($record) use ($request) {
                    return $request->get('start');
                })
                ->addColumn('created', function ($record) use ($request) {
                    $str = '<i class="fa fa-user" title="Creator" data-toggle="tooltip"></i> ' . $record->entryBy() . '<br><i class="fa fa-clock-o" title="Created time" data-toggle="tooltip"></i> ' . $record->created_at->diffForHumans();
                    return $str;
                })
                ->addColumn('action', function ($record) {
                    $btn = '';

                    //Detail
                    $btn .= $this->makeButton([
                        'type' => 'show-page',
                        'id' => $record->id,
                        'datas' => ['url' => route('products.show',$record->id)]
                    ]);

                    //Edit
                    $btn .= $this->makeButton([
                        'type' => 'edit-page',
                        'id' => $record->id,
                        'datas' => ['url' => route('products.edit',$record->id)]
                    ]);

                    // Delete
                    $btn .= $this->makeButton([
                        'type' => 'delete',
                        'id'   => $record->id,
                        'datas' => ['url' => route('products.destroy',$record->id)]
                    ]);

                    return $btn;
                })
                ->rawColumns(['created', 'action'])
                ->make(true);
    }
    
    public function index()
    {        
        return $this->render('modules.products.index', [
            'mockup' => false
        ]);
    }

    public function create()
    {
        $this->setTitle('Create Product');
        $this->pushBreadcrumb(['Create Product' => '#']);
        return $this->render('modules.products.create');
    }

    public function store(ProductRequest $request)
    {
        $product = new Product;
        $product->simpan($request);

        return redirect()->route('products.index')
                        ->with('success','Product created successfully.');
    }

    public function show(Product $product)
    {
        $this->setTitle('Detail Product');
        $this->pushBreadcrumb(['Detail Product' => '#']);
        return $this->render('modules.products.show', [
            'product' => $product
        ]);
    }

    public function edit(Product $product)
    {
        $this->setTitle('Change Product');
        $this->pushBreadcrumb(['Change Product' => '#']);
        return $this->render('modules.products.edit', [
            'product' => $product
        ]);
    }

    public function update(ProductRequest $request, Product $product)
    {
        $product->ubah($request);

        return redirect()->route('products.index')
                        ->with('success','Product updated successfully');
    }

    public function destroy(Product $product)
    {
        $product->hapus();

        return redirect()->route('products.index')
                        ->with('success','Product deleted successfully');
    }
}