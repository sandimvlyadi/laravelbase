<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\User\UserRequest;

use DataTables;

class UserController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:user-list|user-create|user-edit|user-delete', ['only' => ['index','show']]);
        $this->middleware('permission:user-create', ['only' => ['create','store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);

        $this->setPageTitle('User');
        $this->setTitle('User');
        $this->setSubtitle('');
        $this->setBreadcrumb(['User' => '#']);
        $this->setPageLink( url('users') );

        $this->setTableStruct([
            [
			    'data' => 'num',
			    'name' => 'num',
			    'label' => '#',
			    'orderable' => false,
                'searchable' => false,
                'class' => 'text-center',
                'width' => '5%'
			],
            [
			    'data' => 'name',
			    'name' => 'name',
			    'label' => 'Name',
			    'searchable' => false,
			    'sortable' => true
			],
            [
                'data' => 'email',
                'name' => 'email',
                'label' => 'Email',
                'searchable' => false,
                'sortable' => true
            ],
            [
                'data' => 'role',
                'name' => 'role',
                'label' => 'Role',
                'searchable' => false,
                'sortable' => false
            ],
            [
			    'data' => 'action',
			    'name' => 'action',
			    'label' => 'Action',
			    'searchable' => false,
                'sortable' => false,
                'class' => 'text-center',
                'width' => '10%'
			]
        ]);
    }

    public function grid(Request $request)
    {
        $records = User::select('*');

        // Init Sort
        if (!isset(request()->order[0]['column'])) {
            $records->orderBy('id', 'desc');
        }

        // Filters
        if ($name = $request->name) {
            $records->where('name', 'like', '%' . $name . '%');
        }
        if ($email = $request->email) {
            $records->where('email', 'like', '%' . $email . '%');
        }
        if ($role = $request->role) {
            $records->whereHas('roles', function($q) use ($role){
                $q->where('id', $role);
            });
        }

        return Datatables::of($records)
                ->addColumn('num', function ($record) use ($request) {
                    return $request->get('start');
                })
                ->addColumn('role', function ($record) use ($request) {
                    return $record->roleLabel;
                })
                ->addColumn('action', function ($record) {
                    $btn = '';

                    //Detail
                    $btn .= $this->makeButton([
                        'type' => 'show-page',
                        'id' => $record->id,
                        'datas' => ['url' => route('users.show',$record->id)]
                    ]);

                    //Edit
                    $btn .= $this->makeButton([
                        'type' => 'edit-page',
                        'id' => $record->id,
                        'datas' => ['url' => route('users.edit',$record->id)]
                    ]);

                    // Delete
                    if ($record->id != auth()->user()->id) {
                        $btn .= $this->makeButton([
                            'type' => 'delete',
                            'id'   => $record->id,
                            'datas' => ['url' => route('users.destroy',$record->id)]
                        ]);
                    }

                    return $btn;
                })
                ->rawColumns(['role','action'])
                ->make(true);
    }
    
    public function index(Request $request)
    {
        return $this->render('modules.users.index', [
            'mockup' => false
        ]);
    }

    public function create()
    {
        $this->setTitle('Create User');
        $this->pushBreadcrumb(['Create User' => '#']);
        return $this->render('modules.users.create');
    }

    public function store(UserRequest $request)
    {
        $user = new User;
        $user->simpan($request);

        return redirect()->route('users.index')
                        ->with('success','User created successfully.');
    }

    public function show(User $user)
    {
        $this->setTitle('Detail User');
        $this->pushBreadcrumb(['Detail User' => '#']);
        return $this->render('modules.users.show', [
            'record' => $user
        ]);
    }

    public function edit(User $user)
    {
        $this->setTitle('Change User');
        $this->pushBreadcrumb(['Change User' => '#']);
        return $this->render('modules.users.edit', [
            'record' => $user
        ]);
    }

    public function update(UserRequest $request, User $user)
    {
        $user->ubah($request);

        return redirect()->route('users.index')
                        ->with('success','User updated successfully');
    }

    public function destroy(User $user)
    {
        $user->hapus();

        return redirect()->route('users.index')
                        ->with('success','User deleted successfully');
    }
}