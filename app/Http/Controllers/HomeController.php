<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->setPageTitle('Home');
        $this->setTitle('Home');
        $this->setSubtitle('');
        $this->setBreadcrumb(['Home' => '#']);
        $this->setPageLink( url('home') );
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return $this->render('home', [
            'mockup' => true
        ]);
    }
}
