<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Frontend\Message\AdsRequest;
use App\Http\Requests\Frontend\Message\MessageRequest;
use App\Http\Requests\Frontend\Message\SubscribeRequest;

class PageController extends Controller
{
    public function __construct()
    {
        $this->setPageTitle('Page');
        $this->setTitle('Page');
    }

    public function index(Request $request)
    {
        $page = $request->name;
        $data = [];

        switch ($page) {
            case 'articles':
                $this->setPageTitle('Articles');
                $this->setActiveTab('articles');

                return $this->frontend('frontend.articles', [
                    'data' => $data
                ]);
                break;
            case 'article':
                $this->setPageTitle('Article | Lorem ipsum dolor sit amet, consectetur adipiscing elit');
                $this->setActiveTab('articles');

                return $this->frontend('frontend.article', [
                    'data' => $data
                ]);
                break;
            case 'relations':
                $this->setPageTitle('Relations');
                $this->setActiveTab('relations');

                return $this->frontend('frontend.relations');
                break;
            case 'products':
                $this->setPageTitle('Products');
                $this->setActiveTab('products');

                return $this->frontend('frontend.products');
                break;
            case 'contact-me':
                $this->setPageTitle('Contact Me');
                $this->setActiveTab('contact-me');

                return $this->frontend('frontend.contact-me');
                break;
            default:
                abort(404);
                break;
        }
    }

    public function home()
    {
        $this->setPageTitle('Home');
        $this->setActiveTab('home');

        $data = [];
        return $this->frontend('frontend.index', [
            'data' => $data
        ]);
    }

    public function ads(AdsRequest $request)
    {
        dd($request->all());
    }

    public function message(MessageRequest $request)
    {
        dd($request->all());
    }

    public function subscribe(SubscribeRequest $request)
    {
        dd($request->all());
    }
}
