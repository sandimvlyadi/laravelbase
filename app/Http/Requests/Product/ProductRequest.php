<?php

namespace App\Http\Requests\Product;

use App\Http\Requests\Request;

class ProductRequest extends Request
{
    public function rules()
    {
        return [
            'name'      => 'required',
            'detail'    => 'required'
        ];
    }
}
