<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() || !is_null($this->user());
    }

    /**
     * validation message that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required'  => 'Kolom :attribute tidak boleh kosong.',
            'email'     => 'Email tidak valid.',
            'unique'    => 'Kolom :attribute tidak boleh sama.',
            'max'       => 'Kolom :attribute tidak boleh lebih dari :max karakter.',
            'min'       => 'Kolom :attribute tidak boleh kurang dari :min karakter.',
            'same'      => 'Kolom :attribute dan :other harus sama.',
        ];
    }
}