<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        if ($unique != '') {
            return [
                'name'      => 'required',
                'email'     => 'required|email|unique:sys_user,email'.$unique,
                'roles'     => 'required'
            ];
        }

        return [
            'name'      => 'required',
            'email'     => 'required|email|unique:sys_user,email',
            'password'  => 'required|same:confirm-password',
            'roles'     => 'required'
        ];
    }
}
