<?php

namespace App\Http\Requests\Role;

use App\Http\Requests\Request;

class RoleRequest extends Request
{
    public function rules()
    {
        $unique = ($id = $this->get('id')) ? ','.$id : '';

        if ($unique != '') {
            return [
                'name'          => 'required|unique:sys_role,name'.$unique,
                'permission'    => 'required',
            ];
        }

        return [
            'name'          => 'required|unique:sys_role,name',
            'permission'    => 'required'
        ];
    }
}
