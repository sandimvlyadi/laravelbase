<?php

namespace App\Http\Requests\Frontend\Message;

use App\Http\Requests\Request;

class MessageRequest extends Request
{
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        return [
            'type'      => 'required',
            'name'      => 'required',
            'email'     => 'required|email',
            'message'   => 'required'
        ];
    }
}