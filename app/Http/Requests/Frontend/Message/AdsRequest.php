<?php

namespace App\Http\Requests\Frontend\Message;

use App\Http\Requests\Request;

class AdsRequest extends Request
{
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        return [
            'type'              => 'required',
            'business_name'     => 'required',
            'company'           => 'required',
            'business_email'    => 'required|email',
            'contact'           => 'required',
            'description'       => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'business_name'     => 'name',
            'business_email'    => 'email'
        ];
    }
}