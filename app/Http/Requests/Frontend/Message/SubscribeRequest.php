<?php

namespace App\Http\Requests\Frontend\Message;

use App\Http\Requests\Request;

class SubscribeRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'type'              => 'required',
            'email_subscribe'   => 'required|email'
        ];
    }

    public function attributes()
    {
        return [
            'email_subscribe'   => 'email'
        ];
    }
}