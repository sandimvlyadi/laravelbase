<?php

namespace App\Models\Product;

use App\Models\Model;
use App\Models\Logs;

use Carbon\Carbon;

class Product extends Model
{
    protected $table = 'trans_product';

    protected $fillable = [
        'name', 'detail'
    ];

    public function logs()
	{
		return $this->morphMany(Logs::class, 'trans');
	}

    public function simpan($request)
    {
        $this->fill($request->all());
        $this->save();

        $log['action'] = 'create';
        $log['request'] = json_encode($request->all());
        $log['logged_date'] = Carbon::now()->format('Y-m-d h:i:s');
        $log['logged_by'] = auth()->user()->id;
        $save = new Logs;
        $save->fill($log);
        $this->logs()->save($save);
    }

    public function ubah($request)
    {
        $this->fill($request->all());
        $this->save();

        $log['action'] = 'update';
        $log['request'] = json_encode($request->all());
        $log['logged_date'] = Carbon::now()->format('Y-m-d h:i:s');
        $log['logged_by'] = auth()->user()->id;
        $save = new Logs;
        $save->fill($log);
        $this->logs()->save($save);
    }

    public function hapus()
    {
        $log['action'] = 'delete';
        $log['request'] = '-';
        $log['logged_date'] = Carbon::now()->format('Y-m-d h:i:s');
        $log['logged_by'] = auth()->user()->id;
        $save = new Logs;
        $save->fill($log);
        $this->logs()->save($save);

        $this->delete();
    }
}