<?php

namespace App\Models\Traits;

use Carbon\Carbon;

trait Utilities
{
    public static function options($display, $id = 'id', $params = [], $default=null)
    {
        $q = static::select('*')->where($display, '!=', '');
        $params = array_merge([
            'valuePrefix' => '',
        ], $params);

        if (isset($params['filters'])) {
            foreach ($params['filters'] as $key => $value) {
                if (is_numeric($key) && is_callable($value)) {
                    $q = $q->where($value);
                } else {
                    $q = $q->where($key, $value);
                }
            }
        }

        if (isset($params['not-null'])) {
            foreach ($params['not-null'] as $key => $value) {
                if (is_numeric($key)) {
                    $q = $q->whereNotNull($value);
                }
            }
        }

        if (isset($params['not-empty'])) {
            foreach ($params['not-empty'] as $key => $value) {
                if (is_numeric($key)) {
                    $q = $q->where($value, '!=', '');
                }
            }
        }
        
        if (isset($params['not-same'])) {
            foreach ($params['not-same'] as $key => $value) {
                if (is_numeric($key)) {
                    $q = $q->distinct($value);
                }
            }
        }

        if (isset($params['orders'])) {
            foreach ($params['orders'] as $key => $value) {
                if (is_numeric($key)) {
                    $key   = $value;
                    $value = 'asc';
                }

                $q = $q->orderBy($key, $value);
            }
        }

        $r = [];

        $ret = '';
        if ($default !== false) {
            if($default === null){
                $default = '(Pilih Salah Satu)';
            }
            $ret = '<option value="">' . $default . '</option>';
        }

        if (is_string($display)) {
            $q = $q->orderBy($display, 'asc');
            $r = $q->pluck($display, $id);

            foreach ($r as $i => $v) {
                $i = $params['valuePrefix'] . $i;
                $checked = isset($params['selected']) &&
                           (is_array($params['selected']) ? in_array($i, $params['selected']) : $i == $params['selected']);
                if ($checked) {
                    $ret .= '<option value="' . $i . '" selected>' . $v . '</option>';
                } else {
                    $ret .= '<option value="' . $i . '">' . $v . '</option>';
                }
            }
        } elseif (is_callable($display)) {
            $r = $q->get();

            foreach ($r as $d) {
                $i = $params['valuePrefix'] . $d->$id;
                $checked = isset($params['selected']) &&
                           (is_array($params['selected']) ? in_array($i, $params['selected']) : $i == $params['selected']);
                if ($checked) {
                    $ret .= '<option value="' . $i . '" selected>' . $display($d) . '</option>';
                } else {
                    $ret .= '<option value="' . $i . '">' . $display($d) . '</option>';
                }
            }
        }
        return $ret;
    }
}