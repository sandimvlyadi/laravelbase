<?php

namespace App\Models;

use App\Models\Model;
use Carbon\Carbon;

class Logs extends Model
{
    public $timestamps  = false;
    protected $table    = 'sys_logs';

    protected $fillable = [
        'trans_id',
        'trans_type',
        'action',
        'request',
		'logged_date',
        'logged_by'
    ];

    protected $dates = [
        'logged_date',
    ];

	public function trans()
	{
		return $this->morphTo();
    }
}
