<?php

namespace App\Models;

use Spatie\Permission\Models\Role as SpatieRole;
use App\Models\Traits\Utilities;
use App\Models\Logs;

use Carbon\Carbon;
use DB;

class Role extends SpatieRole
{
    use Utilities;

    public function logs()
	{
		return $this->morphMany(Logs::class, 'trans');
	}

    public function perms()
    {
        return DB::table("sys_role_has_permission")->where("sys_role_has_permission.role_id",$this->id)
            ->pluck('sys_role_has_permission.permission_id','sys_role_has_permission.permission_id')
            ->all();
    }

    public function isDeleteable()
    {
        if ($this->users()->count() > 0) {
            return false;
        }

        return true;
    }

    public function simpan($request)
    {
        $this->fill(['name'=>$request->input('name')]);
        $this->save();
        $this->syncPermissions($request->input('permission'));

        $log['action'] = 'create';
        $log['request'] = json_encode($request->all());
        $log['logged_date'] = Carbon::now()->format('Y-m-d h:i:s');
        $log['logged_by'] = auth()->user()->id;
        $save = new Logs;
        $save->fill($log);
        $this->logs()->save($save);
    }

    public function ubah($request)
    {
        $this->fill(['name'=>$request->input('name')]);
        $this->save();
        DB::table('sys_role_has_permission')->where('role_id', $this->id)->delete();
        $this->syncPermissions($request->input('permission'));

        $log['action'] = 'update';
        $log['request'] = json_encode($request->all());
        $log['logged_date'] = Carbon::now()->format('Y-m-d h:i:s');
        $log['logged_by'] = auth()->user()->id;
        $save = new Logs;
        $save->fill($log);
        $this->logs()->save($save);
    }

    public function hapus()
    {
        $log['action'] = 'delete';
        $log['request'] = '-';
        $log['logged_date'] = Carbon::now()->format('Y-m-d h:i:s');
        $log['logged_by'] = auth()->user()->id;
        $save = new Logs;
        $save->fill($log);
        $this->logs()->save($save);

        $this->delete();
    }
}