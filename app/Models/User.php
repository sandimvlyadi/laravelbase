<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;

use Carbon\Carbon;
use Arr;
use DB;
use Hash;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    protected $table = 'sys_user';

    protected $fillable = [
        'name', 'email', 'password', 'photoname', 'photopath'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getRoleLabelAttribute()
    {
        $str = '';
        foreach ($this->roles as $role) {
            $str .= '<span class="label bg-light-blue">'.$role->name.'</span> ';
        }
        return $str;
    }

    public function simpan($request)
    {
        $request['password'] = Hash::make($request['password']);

        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/profile', 'public');
            $request['photopath'] = $path;
            $request['photoname'] = $request->file('photo')->getClientOriginalName();
        }

        $this->fill($request->all());
        $this->save();
        $this->assignRole($request->input('roles'));
    }

    public function ubah($request)
    {
        if(!empty($request['password'])){ 
            $request['password'] = Hash::make($request['password']);
        }else{
            $request = Arr::except($request,array('password'));    
        }

        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('uploads/profile', 'public');
            $request['photopath'] = $path;
            $request['photoname'] = $request->file('photo')->getClientOriginalName();
        }

        $this->fill($request->all());
        $this->save();
        DB::table('sys_model_has_role')->where('model_id', $this->id)->delete();
        $this->assignRole($request->input('roles'));
    }

    public function hapus()
    {
        $this->delete();
    }
}
