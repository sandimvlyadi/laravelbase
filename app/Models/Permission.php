<?php

namespace App\Models;

use Spatie\Permission\Models\Permission as SpatiePermission;
use App\Models\Traits\Utilities;

class Permission extends SpatiePermission
{
    use Utilities;

    public function scopeAllPermission()
    {
        $result = [];
        $permissions = $this::get();
        foreach ($permissions as $perm) {
            $result = array_merge($result, [
                $perm->name => $perm->id
            ]);
        }

        return $result;
    }
}