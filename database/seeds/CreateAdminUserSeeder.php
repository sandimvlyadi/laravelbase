<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
        	'name' => 'Sandi Mulyadi', 
        	'email' => 'sandimvlyadi@gmail.com',
        	'password' => bcrypt('sandiadmin')
        ]);
  
        $role = Role::create(['name' => 'Web Developer']);

        $gr = Role::create(['name' => 'Registered User']);
        $gp = Permission::where('name', 'not like', 'user-%')->where('name', 'not like', 'role-%')->get()->pluck('id','id');
        $gr->syncPermissions($gp);
   
        $permissions = Permission::pluck('id','id')->all();
  
        $role->syncPermissions($permissions);
   
        $user->assignRole([$role->id]);
    }
}
