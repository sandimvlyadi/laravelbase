<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use App\Models\Product\Product;

class DataProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 100; $i++) { 
            Product::create([
                'name'      => 'Product ' . $i,
                'detail'    => 'Detail for product ' . $i
            ]);
        }
    }
}